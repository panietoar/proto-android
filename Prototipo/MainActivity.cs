﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Provider;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using Java.IO;
using Environment = Android.OS.Environment;
using Uri = Android.Net.Uri;
using Android.Locations;
using System.Linq;

namespace Prototipo
{

    public static class App
    {
        public static File _file;
        public static File _dir;
        public static Bitmap bitmap;
    }

    [Activity(Label = "Prototipo", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity, ILocationListener
    {
        private ImageView _imageView;

        LocationManager _locationManager;
        Location _currentLocation;
        string _locationProvider;
        TextView _locationText;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);
            _locationText = FindViewById<TextView>(Resource.Id.textCoords);
            if (SePuedeUsarCamara())
            {
                CrearDirectorioParaImagenes();

                Button button = FindViewById<Button>(Resource.Id.BotonFoto);
                _imageView = FindViewById<ImageView>(Resource.Id.imageView1);
                button.Click += TomarFoto;
                InitializeLocationManager();
                
            }

        }

        void InitializeLocationManager()
        {
            _locationManager = (LocationManager)GetSystemService(LocationService);
            Criteria criteriaForLocationService = new Criteria
            {
                Accuracy = Accuracy.Fine
            };
            IList<string> acceptableLocationProviders = _locationManager.GetProviders(criteriaForLocationService, true);

            if (acceptableLocationProviders.Any())
            {
                _locationProvider = acceptableLocationProviders.First();
            }
            else
            {
                _locationProvider = string.Empty;
            }
        }

        private void CrearDirectorioParaImagenes()
        {
            App._dir = new File(
                Environment.GetExternalStoragePublicDirectory(
                    Environment.DirectoryPictures), "Album Prototipo");
            if (!App._dir.Exists())
            {
                App._dir.Mkdirs();
            }
        }

        private bool SePuedeUsarCamara()
        {
            Intent intent = new Intent(MediaStore.ActionImageCapture);
            IList<ResolveInfo> availableActivities =
                PackageManager.QueryIntentActivities(intent, PackageInfoFlags.MatchDefaultOnly);
            return availableActivities != null && availableActivities.Count > 0;
        } 
        private void TomarFoto(object sender, EventArgs eventArgs)
        {
            Intent intent = new Intent(MediaStore.ActionImageCapture);
            App._file = new File(App._dir, String.Format("foto_{0}.jpg", Guid.NewGuid()));
            intent.PutExtra(MediaStore.ExtraOutput, Uri.FromFile(App._file));
            StartActivityForResult(intent, 0);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            Intent mediaScanIntent = new Intent(Intent.ActionMediaScannerScanFile);
            Uri contentUri = Uri.FromFile(App._file);
            mediaScanIntent.SetData(contentUri);
            SendBroadcast(mediaScanIntent);
            
            int height = Resources.DisplayMetrics.HeightPixels;
            int width = _imageView.Height;
            App.bitmap = App._file.Path.LoadAndResizeBitmap(width, height);
            if (App.bitmap != null)
            {
                _imageView.SetImageBitmap(App.bitmap);
                App.bitmap = null;
            }
            MostrarFormulario();
            GC.Collect();
        }

        private void MostrarFormulario()
        {
            LinearLayout formLayout = FindViewById<LinearLayout>(Resource.Id.linearLayoutForm);
            formLayout.Visibility = ViewStates.Visible;
            Button button = FindViewById<Button>(Resource.Id.buttonEnviar);
            button.Click += EnviarFormulario;
            MostrarCoordenadas();
            Spinner spinnerTerritorios = FindViewById<Spinner>(Resource.Id.spinnerTerritorios);
            var adapter = ArrayAdapter.CreateFromResource(this, Resource.Array.Territorios, Android.Resource.Layout.SimpleSpinnerItem);
            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            spinnerTerritorios.Adapter = adapter;
        }

        private void EnviarFormulario(object sender, EventArgs e)
        {
            var email = new Intent(Android.Content.Intent.ActionSend);

            email.PutExtra(Android.Content.Intent.ExtraEmail,
            new string[] { "panietoar@gmail.com" });

            email.PutExtra(Android.Content.Intent.ExtraSubject, "Formulario ####");

            email.PutExtra(Android.Content.Intent.ExtraText, crearContenidoEmail());
            email.PutExtra(Android.Content.Intent.ExtraStream, Uri.FromFile(App._file));

            email.SetType("message/rfc822");
            StartActivity(email);
        }

        private String crearContenidoEmail()
        {
            String mensaje = "";

            mensaje += "Ubicacion: " + _locationText.Text;
            Spinner spinner = FindViewById<Spinner>(Resource.Id.spinnerTerritorios);
            mensaje += "\nTerritorio: " + spinner.SelectedItem.ToString();
            mensaje += "\nOrganizacion: " + FindViewById<EditText>(Resource.Id.textOrganizacion).Text;
            mensaje += "\nFamilia: " + FindViewById<EditText>(Resource.Id.textFamilia).Text;
            mensaje += "\nProduccion: " + FindViewById<EditText>(Resource.Id.textProduccion).Text;
            mensaje += "\nArea cultivada: " + FindViewById<EditText>(Resource.Id.textAreaCultivada).Text;

            return mensaje;
        }

        private void MostrarCoordenadas()
        {
            if (_currentLocation == null)
            {
                _locationText.Text = "No se puede determinar la ubicacion actual";
                return;
            }

        }

        protected override void OnPause()
        {
            base.OnPause();
            _locationManager.RemoveUpdates(this);
        }

        protected override void OnResume()
        {
            base.OnResume();
            _locationManager.RequestLocationUpdates(_locationProvider, 1000, 0, this);
        }

        public void OnLocationChanged(Location location)
        {
            _currentLocation = location;
            
            if (_currentLocation == null)
            {
                _locationText.Text = "Unable to determine your location. Try again in a short while.";
            }
            else
            {
                _locationText.Text = string.Format("Lat: {0:f4}, Lon: {1:f4}", _currentLocation.Latitude, _currentLocation.Longitude);

            }
        }

        public void OnProviderDisabled(string provider)
        {
        }

        public void OnProviderEnabled(string provider)
        {
        }

        public void OnStatusChanged(string provider, [GeneratedEnum] Availability status, Bundle extras)
        {
        }
    }
}

